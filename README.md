# Web Site Purchase Request

- Start with User profile to give access to user (Employee, Manager or Internal)

Employee Login:
- Login with the employee, in website main screen you will see new tab called 'Create Purchase Request'
- Click Add button to select new product
- Select the vendor, the product and the quantity, you can remove the line or add new line by clickin on add
- Click Submit, if success you will be redirected to Thank you page
- At the backend new record will be created under Website > Purchase Request

Manager Login:
- when the manager login and go to Website > my account > there will be new tab 'Purchase Request'
- Here he should see all PRs
- selecting the PR and click on Confirm


Internal Login:
- On the backend the status of the order now is approved, and employee is get notified with an email
- Internal User can click the button Create PO to create purchase order
