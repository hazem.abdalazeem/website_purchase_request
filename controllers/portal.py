# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo.exceptions import AccessError, MissingError
from collections import OrderedDict
from dateutil.relativedelta import relativedelta
from operator import itemgetter
from datetime import datetime
from odoo import fields, http, _
from odoo.http import request
from odoo.tools import date_utils, groupby as groupbyelem
from odoo.osv.expression import AND, OR
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager


class CustomerPortal(CustomerPortal):

    def _get_searchbar_inputs(self):
        return {
            'all': {'input': 'all', 'label': _('Search in All')},
            'name': {'input': 'name', 'label': _('Search in Description')},
            'employee': {'input': 'employee', 'label': _('Search in Employee')},
            'vendor': {'input': 'vendor', 'label': _('Search in Venor')}
        }

    def _get_search_domain(self, search_in, search):
        search_domain = []

        if search_in in ('name', 'all'):
            search_domain = OR([search_domain, [('name', 'ilike', search)]])
        if search_in in ('employee', 'all'):
            search_domain = OR([search_domain, [('employee_id', 'ilike', search)]])
        if search_in in ('vendor', 'all'):
            search_domain = OR([search_domain, [('partner_id', 'ilike', search)]])
        return search_domain

    def _prepare_home_portal_values(self, counters):
        values = super()._prepare_home_portal_values(counters)
        if 'purchase_request_count' in counters:
            if request.env.user.has_group('website_purchase_request.group_employee'):
                print("request.env.user.id", request.env.user.id)
                values['purchase_request_count'] = request.env['request.puchase.request'].search_count([
                    ('state', 'in', ['draft', 'confirm', 'cancel']), ('employee_id', '=', request.env.user.id)]
                ) if request.env['request.puchase.request'].check_access_rights('read', raise_exception=False) else 0
            else:
                values['purchase_request_count'] = request.env['request.puchase.request'].search_count([
                    ('state', 'in', ['draft', 'confirm', 'cancel'])
                ]) if request.env['request.puchase.request'].check_access_rights('read', raise_exception=False) else 0
                print("values['purchase_request_count']", values['purchase_request_count'])

        return values

    def _purchase_request_order_get_page_view_values(self, order, access_token, **kwargs):
        values = {
            'order': order,
            "is_manager": request.env.user.has_group('website_purchase_request.group_manager')
        }
        return self._get_page_view_values(order, access_token, values, 'my_purchases_request_history', False, **kwargs)

    @http.route(['/my/purchase_request', '/my/purchase_request/page/<int:page>'], type='http', auth="user",
                website=True)
    def portal_my_purchase_orders(self, page=1, date_begin=None, date_end=None, sortby=None, filterby=None, search=None,
                                  search_in='all', groupby='none',
                                  **kw):
        values = self._prepare_portal_layout_values()
        PurchaseOrder = request.env['request.puchase.request']

        domain = []
        if request.env.user.has_group('website_purchase_request.group_employee'):
            domain.append(('employee_id', '=', request.env.user.id))

        if date_begin and date_end:
            domain += [('create_date', '>', date_begin), ('create_date', '<=', date_end)]

        if search and search_in:
            domain += self._get_search_domain(search_in, search)

        searchbar_sortings = {
            'date': {'label': _('Newest'), 'order': 'create_date desc, id desc'},
            'name': {'label': _('Name'), 'order': 'name asc, id asc'},
        }
        # default sort by value
        if not sortby:
            sortby = 'date'
        order = searchbar_sortings[sortby]['order']

        searchbar_filters = {
            'all': {'label': _('All'), 'domain': [('state', 'in', ['draft', 'confirm', 'cancel'])]},
            'purchase': {'label': _('Purchase Request'), 'domain': [('state', '=', 'confirm')]},
            'cancel': {'label': _('Cancelled'), 'domain': [('state', '=', 'cancel')]},
        }

        # default filter by value
        if not filterby:
            filterby = 'all'
        domain += searchbar_filters[filterby]['domain']

        # count for pager
        purchase_request_count = PurchaseOrder.search_count(domain)
        # make pager
        pager = portal_pager(
            url="/my/purchase_request",
            url_args={'date_begin': date_begin, 'date_end': date_end, 'sortby': sortby, 'filterby': filterby},
            total=purchase_request_count,
            page=page,
            step=self._items_per_page
        )
        # search the purchase orders to display, according to the pager data
        orders = PurchaseOrder.search(
            domain,
            order=order,
            limit=self._items_per_page,
            offset=pager['offset']
        )
        request.session['my_purchases_request_history'] = orders.ids[:100]
        searchbar_inputs = self._get_searchbar_inputs()

        values.update({
            'date': date_begin,
            'orders': orders,
            'page_name': 'purchase_request',
            'pager': pager,
            'searchbar_sortings': searchbar_sortings,
            'sortby': sortby,
            'searchbar_filters': OrderedDict(sorted(searchbar_filters.items())),
            'searchbar_inputs': searchbar_inputs,
            'filterby': filterby,
            'search_in': search_in,
            'search': search,
            'default_url': '/my/purchase_request',
        })
        return request.render("website_purchase_request.portal_my_purchase_requests", values)

    @http.route(['/my/purchase_request/<int:order_id>'], type='http', auth="public", website=True)
    def portal_my_purchase_order(self, order_id=None, access_token=None, **kw):
        try:
            order_sudo = self._document_check_access('request.puchase.request', order_id, access_token=access_token)
        except (AccessError, MissingError):
            return request.redirect('/my')

        values = self._purchase_request_order_get_page_view_values(order_sudo, access_token, **kw)

        return request.render("website_purchase_request.portal_my_purchase_request", values)

    @http.route(['/my/purchase_request/confirm/<int:order_id>'], type='http', auth="public", website=True)
    def confirm_my_purchase_order(self, order_id=None, access_token=None, **kw):
        if order_id:
            rfq = request.env['request.puchase.request'].browse(order_id)
            rfq.confirm()
            return request.redirect('/my/purchase_request')
        else:
            return request.redirect('/my/purchase_request')
