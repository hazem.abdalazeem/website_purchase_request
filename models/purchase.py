# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class PruchaseRequest(models.Model):
    _name = 'request.puchase.request'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin']

    name = fields.Char(string="Name", required=False)
    partner_id = fields.Many2one(comodel_name="res.partner", string="Vendor", required=False)
    state = fields.Selection(string="Status",
                             selection=[('draft', 'New'), ('confirm', 'Approved'), ('cancel', 'Refused'), ],
                             default='draft')
    lines = fields.One2many(comodel_name="request.puchase.request.line", inverse_name="request_id", string="Lines",
                            required=False)
    po_id = fields.Many2one(comodel_name="purchase.order", string="PO", required=False)
    date = fields.Date(string="Date", required=False)
    employee_id = fields.Many2one(comodel_name="res.users", string="Employee", required=False)

    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('purchase.request.code') or _('New')
        res = super(PruchaseRequest, self).create(vals)
        return res

    def send_email(self):
        note = "Purchase request has been asked from %s" % self.partner_id.name
        self.send_email_for_user(self.name, note)

    def send_email_for_user(self, name, note):
        mail_content = note
        main_content = {
            'subject': note,
            'author_id': self.env.user.partner_id.id,
            'body_html': mail_content,
            'email_to': self.employee_id.partner_id.email,
            'state': "outgoing",

        }
        res = self.env['mail.mail'].sudo().create(main_content).send()

    def get_acc(self):
        acc = "/my/purchase_request/%s%s" % (self.id, self.get_portal_url())
        return acc

    def confirm(self):
        # self.create_po()
        note = "The request has been approved"
        self.state = 'confirm'
        self.send_email_for_user(self.name, note)

    def cancel(self):
        note = "The request has been rejected"
        self.state = 'cancel'
        self.send_email_for_user(self.name, note)

    def create_po(self):
        values = []
        for line in self.lines:
            values.append((0, 0, {
                'name': 'Test',
                'product_id': line.product_id.id,
                'product_qty': line.qty,
                'product_uom': line.product_id.uom_po_id.id,
                'price_unit': line.product_id.lst_price,
                'date_planned': datetime.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            }))
        pur_res = self.env['purchase.order'].sudo().create({
            'partner_id': self.partner_id.id,
            'date_order': datetime.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
            'order_line': values,
        })
        self.po_id = pur_res.id


class PruchaseRequestLine(models.Model):
    _name = 'request.puchase.request.line'

    product_id = fields.Many2one(comodel_name="product.product", string="Product", required=False)
    qty = fields.Integer(string="Quantity", required=False)
    request_id = fields.Many2one(comodel_name="request.puchase.request", )
